#################
## Historical trials (EBV)
#################
# This script demonstrates how to calculate the realized rate of genetic gain using historical 
# data that has been collected from trials that have NOT followed recommendations to maximize 
# the connectivity and TPE coverage. This represents the initial state of many programs to 
# measure the rate of genetic gain. The number coming from this type of analysis has to be 
# considered only as a baseline to provide some insight to funding and collaborator institutions 
# on the trends of the program.
## DATA: historical dataset from PYT|AYT|ELITE together with pedigree information

library(lme4)
library(ggplot2)
library(asreml)

# assuming you have downloaded the project folder in your Desktop we set it as the working directory
setwd("~/Desktop/breeding-program-assessment/")

deltaTrueCor <- deltaEstCor <- numeric() # store true and estimated genetic gain for this iteration so we can plot
deltaEstimated <- deltaTrue <- list() # to store the increase in genetic gain for iterations
evolEstimated <- evolTrue <- list() # to store the increase in genetic gain for iterations

for(REP in 1:15){
  print(paste("REP", REP))
  load(file.path("simulated-data",paste0("BASE_",REP,".RData"))) # load data from simulated program
  
  ########################
  # define time-window and 
  # germplasm sample: "Elite", "Stage3", "Stage2", "Stage1"
  ########################
  gto=21:40 # years of data to use
  useData <- yearData[which((yearData$year %in% gto) & (yearData$stage %in% c("Stage3"))),] # filter
  
  ########################
  # prepare the dataset
  ########################
  cp <- useData # rename to avoid confusions later; head(cp); dim(cp);table(cp$env, cp$year)
  years <- unique(cp$year)
  table(cp$planted);table(cp$type)
  
  finalData <- droplevels(cp[which(cp$planted == 1),]) # keep only environments sampled in the simulation
  finalData <- droplevels(finalData[which(finalData$type %in% c("Forward")),]) # subset to material of interest, we only keep the breeding material
  finalData$yearf <- as.factor(finalData$year) # convert year to factor
  finalData$typef <- as.factor(finalData$type) # convert material type to factor
  finalData$new <- ifelse(finalData$type == "Forward", 1, 0) # create a new column to indicate if is a check or not
  finalData$entryc <- "noCheck"; # new column to indicate if is a check or not as a character column
  finalData$entryc[which(finalData$new == 0)] <- as.character(finalData$id[which(finalData$new == 0)])
  finalData$entryc <- as.factor(finalData$entryc) # convert to factor
  finalData$yearOrigin <- finalData$new * finalData$yearOrigin # revome year of origin for checks
  finalData <- finalData[with(finalData, order(-envn)), ] # sort data by environments
  table(finalData$env, finalData$year)
  
  ########################
  # estimated genetic gain
  ########################
  
  library(asreml)
  pdfg <- unique(finalData[,c("id","mom","dad","generation")]); head(pdfg) # prepare information for pedigree
  pdfg.ai <- ainverse(pdfg,fgen=list('generation',0.99)) # calculate additive relationship matrix
  pdfg.mat <- sp2mat(pdfg.ai) # convert to matrix in case you want to visualize the A matrix
  # image(as(zapsmall(solve(pdfg.mat)), Class="sparseMatrix")) # image of A
  
  head(finalData)
  mix.var <- asreml(p~ year + yearf,
                    random= ~ env + yearf:env + 
                      # rr(yearf):vm(id, source=pdfg.ai) + diag(yearf):vm(id, source=pdfg.ai) # other GxE model
                      vm(id, source=pdfg.ai) #+ diag(yearf):vm(id, source=pdfg.ai)
                    ,
                    residual=~units,#dsum(~units | env),
                    data=droplevels(finalData), 
                    maxiter=50, workspace=250000000
  )
  base <- unique(finalData[,c("id","yearOrigin")]) # extract year of origin from the material to merge later
  predictions <- predict(mix.var, classify = "id")#, predict across year and environment means # pworkspace=250000000, workspace=250000000)
  predictions2 <- predictions$pvals # keep predictions
  predictions3 <- merge(predictions2, base, by="id") # add year of origin to predictions
  predictions3 <- predictions3[which(predictions3$yearOrigin >0),] # remove checks
  
  mix.var2 <- lm(predicted.value~yearOrigin, data=predictions3)  # fit the second model
  b2=summary(mix.var2)$coefficients[2,1]; b2 # extract the rate of genetic gain
  deltaEstCor[REP] <- b2 # save the rate of genetic gain
  
  prov <- aggregate(predicted.value~yearOrigin, FUN=mean, data=predictions3); # aggregate data to store
  prov$group <- "Estimated"; colnames(prov)[2] <- "g" # provide additional indicators
  deltaEstimated[[REP]] <- prov # store estimated rate of gain
  
  
  ###################
  # true genetic gain
  ###################
  cp2 <- droplevels(cp[which(cp$type %in% c("Forward")),]) # keep only forward breeding material (no checks)
  cp2 <- aggregate(g~id+yearOrigin,data=cp2, FUN=mean) # aggregate by genotype and year of origin to remove extra reps using teh real genetic vale
  
  mix.var.true <- lm(g~yearOrigin, data=cp2) # fit the linear model
  b2=summary(mix.var.true)$coefficients[2,1]; b2 # extract the true rate of genetic gain
  deltaTrueCor[REP] <- b2 # store the true rate of genetic gain
  
  prov <- aggregate(g~yearOrigin, FUN=mean, data=cp2); # agregate to store the data
  prov$group <- "True" # additional indicator
  deltaTrue[[REP]] <- prov # store
  
  ###################
  # estimated evolution of genetic variance
  ###################
  est.evol <- aggregate(predicted.value~yearOrigin, FUN=var, data=predictions3); est.evol$group <- "Estimated" # use across year:env  genotype means to calculate variance by year of origin
  colnames(est.evol)[2] <- "g" # change name from predicted to "g"
  evolEstimated[[REP]] <- est.evol # store information
  
  ###################
  # true evolution of genetic variance
  ###################
  cp2 <- droplevels(cp[which(cp$type %in% c("Forward")),]) # take data before filtering planted environments
  cp2$yearf <- as.factor(cp2$year) # change to factor
  cp2$yearOriginF <- as.factor(cp2$yearOrigin) # change to factor
  cp2 <- cp2[with(cp2, order(-yearOrigin)), ] # sort data by year of origin
  
  true.evol <- aggregate(g~yearOriginF, FUN=var, data=cp2) # aggregate data to have true variance by year of origin
  true.evol$yearOrigin <- as.numeric(as.character(true.evol$yearOriginF)); true.evol$group <- "True" # additional indicators
  evolTrue[[REP]] <- true.evol[, colnames(est.evol)] # store results of true variance
  
} # END OF LOOP


###################
# correlation plot among true and estimated genetic gain
###################
plot(x=deltaTrueCor, y=deltaEstCor, xlab="True genetic gain", ylab="Estimated genetic gain",
     main="Comparison of true versus estimated genetic gain", cex=2, pch=20, col="black"
      ,xlim=c(0,0.2), ylim=c(0,0.2) 
)
abline(0,1, lty=3, col="red", lwd=2)

###################
# correlation plot among true and estimated evolution of genetic variance
###################
plot(x=do.call(rbind, evolTrue )$g, y=do.call(rbind, evolEstimated)$g, xlab="True genetic variance", ylab="Estimated genetic variance",
     main="Comparison of true versus estimated genetic variance", cex=2, pch=20, col="black"
     ,xlim=c(0,1.2), ylim=c(0,1.2)  
) # to add by year coloring?
abline(0,1, lty=3, col="red", lwd=2)

###################
# estimated & true genetic gain plot
###################
prov1 <- do.call(rbind, deltaEstimated)
prov2 <- do.call(rbind, deltaTrue)
prov <- rbind(prov1,prov2)

a <- aggregate(g~yearOrigin+group, data=prov, FUN=mean)
b <- aggregate(g~yearOrigin+group, data=prov, FUN=function(x){sqrt(var(x))/sqrt(length(x))}); colnames(b)[3] <- "se"
ab <- merge(a,b, by=c("group","yearOrigin"))
p <- ggplot(ab, aes(x=yearOrigin, y=g, color = group)) +
  geom_line(aes(x=yearOrigin, y=g, color=group)) +
  geom_ribbon(aes(ymin=g-se,ymax=g+se,fill=group),color="grey70",alpha=0.4) + ylab("Genetic value") + xlab("Year of origin")+
  ylim(2,16)
print(p)

###################
# estimated & true evolution of genetic variance plot
###################
prov1 <- do.call(rbind, evolEstimated)
prov2 <- do.call(rbind, evolTrue)
prov <- rbind(prov1,prov2)

a <- aggregate(g~yearOrigin+group, data=prov, FUN=mean)
b <- aggregate(g~yearOrigin+group, data=prov, FUN=function(x){sqrt(var(x))/sqrt(length(x))}); colnames(b)[3] <- "se"
ab <- merge(a,b, by=c("group","yearOrigin"))
p <- ggplot(ab, aes(x=yearOrigin, y=g, color = group)) +
  geom_line(aes(x=yearOrigin, y=g, color=group)) +
  geom_ribbon(aes(ymin=g-se,ymax=g+se,fill=group),color="grey70",alpha=0.4) + ylab("Genetic variance") + xlab("Year of origin") +
  ylim(0,0.6)
print(p)



