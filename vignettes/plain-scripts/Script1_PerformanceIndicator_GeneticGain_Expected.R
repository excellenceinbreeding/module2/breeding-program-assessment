################################
## Expected/predicted rate of genetic gain
################################
# This script demonstrates how to calculate the expected/predicted rate of genetic gain using 
# two parameterizations of the breeders' equation. This should only be used as a quick assessment 
# to know if response can be expected but not for deriving a number that should be shared with 
# funding or collaborating entities.

# original population
n=1000
m.original=5
var.p.original=2
var.g.original=1
var.e.original=1
h2=var.g.original/var.p.original; h2
cycle.time=5 #years

x <- rnorm(n=n,mean=m.original,sd=sqrt(var.p.original))
x <- sort(x, decreasing = TRUE)

# selected fraction
sel.prop=0.1
x.selected <- x[1:(n*sel.prop)]
m.selected <- mean(x.selected); m.selected

plot(density(x), xlim=c(1,10),main="", xlab="")
par(new=T)
plot(density(x.selected), col="blue", xlim=c(1,10), ylab="", yaxt="n")

# expected response to selection using R=h2*S
S=m.selected - m.original; S
R=h2*S;R

# expected response to selection using R=i*r*sigma
i=(dnorm(qnorm(1 - sel.prop))/sel.prop);i
r=sqrt(h2);r
R=i*r*var.g.original;R

# rate of genetic gain
R/cycle.time
