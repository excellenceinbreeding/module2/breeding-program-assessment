# Breeding Program Assessment

As any data-driven institution, the CGIAR requires KPIs to condense breeding data into meaningful metrics that allow breeding programs to assess the overall performance. The variety turnover and genetic gain have been proposed as two key metrics. In this GitLab page we provide scripts and data to recreate examples mentioned in the "how to" manual "Overall_breeding_program_assessment.pdf" available in the toolbox of EiB (https://excellenceinbreeding.org/toolbox/tools/eib-breeding-scheme-optimization-manuals). Please refer to this file to understand the scripts and data stored in this project.

## literature folder

Contains the papers reviewed to develop the "overall_breeding_program_assessment.pdf" file.

## vignettes folder

Contains manuals to show the calculation of genetic gain with different methods and plain scripts to recreate the results and pictures from the manual:

1) Expected/predicted (**ONLY FOR QUICK CHECK**)

2) Realized
  2.1) Historical trials
     2.1.1) Good connectivity scenario (**PREFERRED METHOD**)
     2.1.2) Bad connectivity scenario (EBV method; **AVOID**)
  2.2) Era trials (**ONLY WHILE 2.1.1 IS IMPLEMENTED**)
  
geneticGainCalculation_Predicted

This script demonstrates how to calculate the expected/predicted rate of genetic gain using two parameterizations of the breeders' equation. This should only be used as a quick assessment to know if response can be expected but not for deriving a number that should be shared with funding or collaborating entities.


geneticGainCalculation_Historical

This script demonstrates how to calculate the realized rate of genetic gain using historical data that has been collected from trials using recommendations to maximize the connectivity and TPE coverage. This represents the ideal state to measure the rate of genetic gain. The number coming from this type of analysis can be shared with funding and collaborator institutions.


geneticGainCalculation_Historical_EBV

This script demonstrates how to calculate the realized rate of genetic gain using historical data that has been collected from trials that have NOT followed recommendations to maximize the connectivity and TPE coverage. This represents the initial state of many programs to measure the rate of genetic gain. The number coming from this type of analysis has to be considered only as a baseline to provide some insight to funding and collaborator institutions on the trends of the program.


geneticGainCalculation_Era

This script demonstrates how to calculate the realized rate of genetic gain using trial data from a formal era trial experiment that that followed good experimental design practices to maximize accuracy of the estimates. This represents the initial state of many programs to measure the rate of genetic gain that do not have historical information or distrust of it. The number coming from this type of analysis can be considered a baseline or an accurate estimate of this KPI to be shared with funding and collaborator institutions.

## simulated-data folder

Contains the Rdata objects required to run the scripts that recreate the different methods. No need to review, is only used to load data to run the examples.

BASE_X.RData contains the results (phenotypic data) from N parallel breeding programs running under the same conditions.  X refers to STAGE1, STAGE2, STAGE3, ELITE germplasm samples.

POP_BASE_X.RData contains the ... X refers to STAGE1, STAGE2, STAGE3, ELITE germplasm samples.

BASE_ERA_X.RData contains the AlphaSimR population objects for the different era trials run from N parallel breeding programs run under the same conditions. X refers to STAGE1, STAGE2, STAGE3, ELITE germplasm samples.

BURNIN.R script to initialize the simulated breeding program with specific features. Returns the objects POP_BASE_X.RData 

RUN_PROGRAM.R script to take the BURNIN.R results and run a breeding program for a specified number of years. Returns the objects BASE_X.RData

RUN_ERA.R script used to load the populations from different years and simulate as we were running the era trials. It returns the objects BASE_ERA_X.RData

## overall_breeding_program_assessment.pdf

Contains the recommendations from Excellence in Breeding (EiB) to design experiments properly to be able to use the historical trial information to calculare the rate of genetic gain with high accuracy and low cost.