# Install packages needed later
# install.packages(c("ggplot2","tibble","dplyr"))

path0 <- "/home/gcovarrubias"#"Z:"
path <- file.path(path0,"breeding-program-assessment/check-strategy/data");path
# path <- file.path("Z:","simulation-cgiar-library/ciat-beans/latin-america/ip1-size/data-grid");path

library(AlphaSimR, lib.loc = file.path(path0,"R_LIBS36"))
library(utils, lib.loc = file.path(path0,"R_LIBS"))

simulateGECorrMat <- function (nEnv, nMegaEnv, mu = 0.7, v = 0.2){
  ff <- function(m) {
    m[lower.tri(m)] <- t(m)[lower.tri(m)]
    m
  }
  G = matrix(NA, nEnv, nEnv)
  (nEnv2 <- nEnv/nMegaEnv)
  G
  starts <- seq(1, nEnv, nEnv/nMegaEnv)
  ends <- c((starts - 1)[-1], nEnv)
  for (i in 1:nMegaEnv) {
    corsprov <- rnorm((nEnv2 * (nEnv2 - 1))/2, mu, v)
    counter = 1
    for (j in starts[i]:ends[i]) {
      for (k in j:ends[i]) {
        i
        if (j == k) {
          G[j, k] <- 1
        }
        else {
          G[j, k] <- corsprov[counter]
          counter <- counter + 1
        }
      }
    }
  }
  G <- ff(G)
  tofill <- which(is.na(G), arr.ind = TRUE)
  G[tofill] <- rnorm(nrow(tofill), 0, 0.3)
  G[which((G) > 1)] <- 0.98
  G[which((G) < -1)] <- -0.98
  G <- ff(G)
  return(G)
}

# setwd("~/Desktop/breeding-program-assessment/simulated_data")

# Run 10 replications of the simulation
for(REP in 21:100){
  print(paste("REP",REP))
  ## Create founder haplotypes
  founderPop = runMacs2(nInd=50, nChr=10, segSites=1000+1000, Ne=30)
  
  ## Set simulation parameters
  SP = SimParam$new(founderPop)
  
  nEnvironments=20 # control number of environments
  m <- simulateGECorrMat(nEnv=nEnvironments,nMegaEnv=1, mu=0.35, v=0.1)   # control average genetic correlation
  
  SP$addTraitA(nQtlPerChr = 1000, mean = rep(0,nEnvironments), var = rep(1,nEnvironments),corA=m ) # Add two GxE traits
  SP$setVarE(varE = rep(2,nEnvironments)) # Set default error variances # control heritability
  SP$setTrackPed(TRUE)
  SP$addSnpChip(100)
  SP$setGender("no")
  # econWt = rep(1, nEnvironments)
  
  ## Fill breeding program pipeline
  Parents = newPop(founderPop)
  for(u in 1:9){ # self parents
    Parents <- self(Parents, nProgeny = 1, parents = NULL, simParam = NULL)
  } # 50 parents
  
  Checks = newPop(founderPop); Checks = selectInd(Checks, 20,  use="rand", simParam=SP)
  for(u in 1:9){
    Checks <- self(Checks, nProgeny = 1, parents = NULL, simParam = NULL)
  }; nChecks <- length(Checks@id)
  
  Control <- selectInd(Parents, nInd = 20, use="rand" )
  nControl <- length(Control@id)
  
  # Create genotypes from year 7 (Elite)
  F1 = randCross(Parents, nCrosses=100, nProgeny=20) # 2000 genotypes
  F2F3 = F1 # year 1
  for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
  F4F5 = F2F3
  for(u in 1:2){F4F5 <- self(F4F5, nProgeny = 1, parents = NULL, simParam = NULL)} # year 3
  
  Stage1 = setPheno(c(Checks, Control, F4F5), reps=1, varE=rep(4,nEnvironments)) # year 4
  econWt1 = rep(0, nEnvironments); econWt1[sample(1:nEnvironments,1)] <- 1; econWt1 
  b = smithHazel(econWt1, varG(Stage1), varP(Stage1))
  Stage2 = selectInd(Stage1, nInd=200, candidates= (nControl+nChecks+1):length(Stage1@id), trait=selIndex, b=b)
  
  Stage2 = setPheno(c(Checks, Control, Stage2), reps=2, varE=rep(4,nEnvironments)) # year 5
  econWt2 = rep(0, nEnvironments); econWt2[sample(1:nEnvironments,3)] <- 1; econWt2
  b = smithHazel(econWt2, varG(Stage2), varP(Stage2))
  Stage3 = selectInd(Stage2, nInd=50, candidates= (nControl+nChecks+1):length(Stage2@id), trait=selIndex, b=b)
  
  Stage3 = setPheno(c(Checks, Control, Stage3), reps=3, varE=rep(4,nEnvironments)) # year 6
  econWt3 = rep(0, nEnvironments); econWt3[sample(1:nEnvironments,8)] <- 1; econWt3
  b = smithHazel(econWt3, varG(Stage3), varP(Stage3))
  Elite = selectInd(Stage3, nInd=5,candidates= (nControl+nChecks+1):length(Stage3@id), trait=selIndex, b=b)
  
  Elite = setPheno(c(Checks, Control, Elite), reps=4, varE=rep(4,nEnvironments)) # year 7
  econWt4 = rep(0, nEnvironments); econWt4[sample(1:nEnvironments,20)] <- 1; econWt4

  # Create genotypes from year 6 (Stage 3)
  F1 = randCross(Parents, nCrosses=100, nProgeny=20) # 2000 genotypes
  F2F3 = F1 # year 1
  for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
  F4F5 = F2F3
  for(u in 1:2){F4F5 <- self(F4F5, nProgeny = 1, parents = NULL, simParam = NULL)} # year 3
  
  Stage1 = setPheno(c(Checks, Control, F4F5), reps=1, varE=rep(4,nEnvironments)) # year 4
  econWt1 = rep(0, nEnvironments); econWt1[sample(1:nEnvironments,1)] <- 1; econWt1 
  b = smithHazel(econWt1, varG(Stage1), varP(Stage1))
  Stage2 = selectInd(Stage1, nInd=200, candidates= (nControl+nChecks+1):length(Stage1@id), trait=selIndex, b=b)
  
  Stage2 = setPheno(c(Checks, Control, Stage2), reps=2, varE=rep(4,nEnvironments)) # year 5
  econWt2 = rep(0, nEnvironments); econWt2[sample(1:nEnvironments,3)] <- 1; econWt2
  b = smithHazel(econWt2, varG(Stage2), varP(Stage2))
  Stage3 = selectInd(Stage2, nInd=50, candidates= (nControl+nChecks+1):length(Stage2@id), trait=selIndex, b=b)
  
  Stage3 = setPheno(c(Checks, Control, Stage3), reps=3, varE=rep(4,nEnvironments)) # year 6
  econWt3 = rep(0, nEnvironments); econWt3[sample(1:nEnvironments,8)] <- 1; econWt3
  
  # Create genotypes from year 5 (Stage 2)
  F1 = randCross(Parents, nCrosses=100, nProgeny=20) # 2000 genotypes
  F2F3 = F1 # year 1
  for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
  F4F5 = F2F3
  for(u in 1:2){F4F5 <- self(F4F5, nProgeny = 1, parents = NULL, simParam = NULL)} # year 3
  
  Stage1 = setPheno(c(Checks, Control, F4F5), reps=1, varE=rep(4,nEnvironments)) # year 4
  econWt1 = rep(0, nEnvironments); econWt1[sample(1:nEnvironments,1)] <- 1; econWt1 
  b = smithHazel(econWt1, varG(Stage1), varP(Stage1))
  Stage2 = selectInd(Stage1, nInd=200, candidates= (nControl+nChecks+1):length(Stage1@id), trait=selIndex, b=b)
  
  Stage2 = setPheno(c(Checks, Control, Stage2), reps=2, varE=rep(4,nEnvironments)) # year 5
  econWt2 = rep(0, nEnvironments); econWt2[sample(1:nEnvironments,3)] <- 1; econWt2
  
  # Create genotypes from year 4 (Stage 1)
  F1 = randCross(Parents, nCrosses=100, nProgeny=20) # 2000 genotypes
  F2F3 = F1 # year 1
  for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
  F4F5 = F2F3
  for(u in 1:2){F4F5 <- self(F4F5, nProgeny = 1, parents = NULL, simParam = NULL)} # year 3
  
  Stage1 = setPheno(c(Checks, Control, F4F5), reps=1, varE=rep(4,nEnvironments)) # year 4
  econWt1 = rep(0, nEnvironments); econWt1[sample(1:nEnvironments,1)] <- 1; econWt1 
  
  # Create genotypes from year 3 (F4-F5)
  F1 = randCross(Parents, nCrosses=100, nProgeny=20) # 2000 genotypes
  F2F3 = F1 # year 1
  for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
  F4F5 = F2F3
  for(u in 1:2){F4F5 <- self(F4F5, nProgeny = 1, parents = NULL, simParam = NULL)} # year 3
  
  # Create genotypes from year 2 (F2-F3)
  F1 = randCross(Parents, nCrosses=100, nProgeny=20) # 2000 genotypes
  F2F3 = F1 # year 1
  for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
  
  # Crossing and germination of F1 year 1
  F1 = randCross(Parents, nCrosses=100, nProgeny=20) # 2000 genotypes
  
  ## Simulate 20 years of burn-in
  for(year in 1:20){
    print(year)
    # new parents
    b = smithHazel(econWt3, varG(Stage3), varP(Stage3))
    ParentsElite <- selectInd(Stage3, nInd=5,candidates= (nControl+nChecks+1):length(Stage3@id), trait=selIndex, b=b)
    
    b = smithHazel(econWt2, varG(Stage2), varP(Stage2))
    ParentsStage3 <- selectInd(Stage2, nInd=45, candidates= (nControl+nChecks+1):length(Stage2@id), trait=selIndex, b=b)
    
    Parents = c(ParentsElite, ParentsStage3)
    # refresh checks
    toRefresh=1
    Checks <- c(Checks[-c(1:toRefresh)],Elite[sample(1:length(Elite@id),toRefresh)])
    
    # Year 7 (Elite)
    b = smithHazel(econWt3, varG(Stage3), varP(Stage3))
    Elite = selectInd(Stage3, nInd=5,candidates= (nControl+nChecks+1):length(Stage3@id), trait=selIndex, b=b)
    Elite = setPheno(c(Checks, Control, Elite), reps=4, varE=rep(4,nEnvironments))
    econWt4 = rep(0, nEnvironments); econWt4[sample(1:nEnvironments,20)] <- 1; econWt4
    
    # Year 6 (Stage 3)
    b = smithHazel(econWt2, varG(Stage2), varP(Stage2))
    Stage3 = selectInd(Stage2, nInd=45, candidates= (nControl+nChecks+1):length(Stage2@id), trait=selIndex, b=b)
    Stage3 = setPheno(c(Checks, Control, Stage3), reps=3, varE=rep(4,nEnvironments)) 
    econWt3 = rep(0, nEnvironments); econWt3[sample(1:nEnvironments,8)] <- 1; econWt3
    
    # Year 5 (Stage 2)
    b = smithHazel(econWt1, varG(Stage1), varP(Stage1))
    Stage2 = selectInd(Stage1, nInd=200, candidates= (nControl+nChecks+1):length(Stage1@id), trait=selIndex, b=b)
    Stage2 = setPheno(c(Checks, Control, Stage2), reps=2, varE=rep(4,nEnvironments))
    econWt2 = rep(0, nEnvironments); econWt2[sample(1:nEnvironments,3)] <- 1; econWt2
    
    # Year 4 (Stage 1)
    Stage1 = setPheno(c(Checks, Control, F4F5), reps=1, varE=rep(4,nEnvironments))
    econWt1 = rep(0, nEnvironments); econWt1[sample(1:nEnvironments,1)] <- 1; econWt1
    
    # Year 3 (F4F5)
    F4F5 = F2F3
    for(u in 1:2){F4F5 <- self(F4F5, nProgeny = 1, parents = NULL, simParam = NULL)} # year 3
    
    # Year 2 (F2F3)
    F2F3 = F1 # year 1
    for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
    
    # Year 1 (Crossing)
    F1 = randCross(Parents, nCrosses=100, nProgeny=20)

  }
  
  ## Save global environment
  save.image(file=file.path(path,paste0("BURNIN_",REP,".RData")), version = 2)
  # save.image(paste0("BURNIN_",REP,".RData"))
}



