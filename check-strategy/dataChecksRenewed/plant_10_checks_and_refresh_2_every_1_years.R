path0 <- "/home/gcovarrubias"#"Z:"

library(AlphaSimR, lib.loc = file.path(path0,"R_LIBS36"))
library(utils, lib.loc = file.path(path0,"R_LIBS"))

makeDF <- function(pop){
  g=apply(pop@gv,1,mean)
  mes <- abs(rnorm(ncol(pop@gv),sample(4:10,1),3)) # mean 4-10 for environments
  pop@pheno <- sweep(pop@pheno, 2, mes, "+")  # add an environmental mean
  # e=apply(pop@pheno,2,mean)
  
  phenoList <- list()
  for(ienv in 1:ncol(pop@gv)){
    g0=g
    ge0=pop@gv[,ienv]
    e0=rep(mes[ienv], length(g0))
    p=pop@pheno[,ienv]
    er=p-(e0+ge0)
    df <- data.frame(id=pop@id, mom=pop@mother, dad=pop@father,  generation=10, idn=as.numeric(as.character(pop@id)),env=paste0("e",ienv), envn=ienv, g=g0, e=e0, ge=ge0, error=er, p=p)
    phenoList[[ienv]] <- df#rbind(df,df2)
  }
  toAdd <- do.call(rbind,phenoList)
  return(toAdd)
}

# Run 10 replications of the simulation
for(REP in 1:200){ # REP=1
  
  path <- file.path(path0,"breeding-program-assessment/check-strategy/dataChecksRenewed");path
  load(file.path(path,paste0("BURNIN_",REP,".RData")))
  
  yearData <- list(); counter <- 1
  ParentsContainer <- Stage1Container <- Stage2Container <- Stage3Container <- EliteContainer <- list()
  
  nc=10 # decide number of checks
  ny=1; ny2 <- seq(1,100,ny) # how often in years should we refresh the checks
  toRefresh=2 # how many checks to refresh
  
  scenarioName = paste0("plant_",nc,"_checks_and_refresh_",toRefresh,"_every_",ny,"_years")
  ## select #of checks for this tratment
  ChecksNames <- Checks@id
  Checks <- Checks[sample(1:length(Checks@id),nc)]
  ControlNames <- Control@id
  '%!in%' <- function(x,y)!('%in%'(x,y)) 
  
  ## Simulate years 21 through 40
  for(year in 21:50){ # year=21
    print(year)
    # new parents
    b = smithHazel(econWt3, varG(Stage3), varP(Stage3))
    ParentsElite <- selectInd(Stage3, nInd=5,candidates= which(Stage3@id %!in% c(ControlNames,ChecksNames)), trait=selIndex, b=b)
    
    b = smithHazel(econWt2, varG(Stage2), varP(Stage2))
    ParentsStage3 <- selectInd(Stage2, nInd=45, candidates= which(Stage2@id %!in% c(ControlNames,ChecksNames)), trait=selIndex, b=b)
    
    Parents = c(ParentsElite, ParentsStage3)
    ParentsContainer[[counter]] <- Parents
    # refresh checks or not
    
    if(year %in% ny2){# if enough time passed refresh checks
      Checks <- c(Checks[-c(1:toRefresh)],Elite[sample(1:length(Elite@id),toRefresh)])
      ChecksNames <- unique(ChecksNames,Checks@id)
    }
    
    
    # Year 7 (Elite)
    b = smithHazel(econWt3, varG(Stage3), varP(Stage3))
    Elite = selectInd(Stage3, nInd=5,candidates= which(Stage3@id %!in% c(ControlNames,ChecksNames)), trait=selIndex, b=b)
    Elite = setPheno(c(Checks, Control, Elite), reps=4, varE=rep(4,nEnvironments))
    econWt4 = rep(0, nEnvironments); econWt4[sample(1:nEnvironments,20)] <- 1; econWt4
    
    EliteData <- makeDF(Elite); EliteData$planted <- 0; EliteData$planted[EliteData$envn %in% which(econWt4 == 1)] <- 1; EliteData$year <- year; EliteData$stage <- "Elite"; 
    EliteData$type <- "Forward";     EliteData$type[which(EliteData$id %in% Control@id)]="Control";    EliteData$type[which(EliteData$id %in% Checks@id)]="Checks";
    EliteData$yearOrigin <- year - 7
    EliteContainer[[counter]] <- Elite
    
    # Year 6 (Stage 3)
    b = smithHazel(econWt2, varG(Stage2), varP(Stage2))
    Stage3 = selectInd(Stage2, nInd=45, candidates= which(Stage2@id %!in% c(ControlNames,ChecksNames)), trait=selIndex, b=b)
    Stage3 = setPheno(c(Checks, Control, Stage3), reps=3, varE=rep(4,nEnvironments)) 
    econWt3 = rep(0, nEnvironments); econWt3[sample(1:nEnvironments,8)] <- 1; econWt3
    
    Stage3Data <- makeDF(Stage3); 
    Stage3Data$planted <- 0; 
    Stage3Data$planted[Stage3Data$envn %in% which(econWt3 == 1)] <- 1; 
    Stage3Data$year <- year; 
    Stage3Data$stage <- "Stage3"; 
    Stage3Data$type <- "Forward";   Stage3Data$type[which(Stage3Data$id %in% Control@id)]="Control";    Stage3Data$type[which(Stage3Data$id %in% Checks@id)]="Checks";
    Stage3Data$yearOrigin <- year - 6
    Stage3Container[[counter]] <- Stage3
    
    # Year 5 (Stage 2)
    b = smithHazel(econWt1, varG(Stage1), varP(Stage1))
    Stage2 = selectInd(Stage1, nInd=200, candidates= which(Stage1@id %!in% c(ControlNames,ChecksNames)), trait=selIndex, b=b)
    Stage2 = setPheno(c(Checks, Control, Stage2), reps=2, varE=rep(4,nEnvironments))
    econWt2 = rep(0, nEnvironments); econWt2[sample(1:nEnvironments,3)] <- 1; econWt2
    
    Stage2Data <- makeDF(Stage2); Stage2Data$planted <- 0; Stage2Data$planted[Stage2Data$envn %in% which(econWt2 == 1)] <- 1; Stage2Data$year <- year; Stage2Data$stage <- "Stage2"; 
    Stage2Data$type <- "Forward";     Stage2Data$type[which(Stage2Data$id %in% Control@id)]="Control";    Stage2Data$type[which(Stage2Data$id %in% Checks@id)]="Checks"; 
    Stage2Data$yearOrigin <- year - 5
    Stage2Container[[counter]] <- Stage2
    
    # Year 4 (Stage 1)
    Stage1 = setPheno(c(Checks, Control, F4F5), reps=1, varE=rep(4,nEnvironments))
    econWt1 = rep(0, nEnvironments); econWt1[sample(1:nEnvironments,1)] <- 1; econWt1
    
    Stage1Data <- makeDF(Stage1); Stage1Data$planted <- 0; Stage1Data$planted[Stage1Data$envn %in% which(econWt1 == 1)] <- 1; Stage1Data$year <- year; Stage1Data$stage <- "Stage1"; 
    Stage1Data$type <- "Forward";     Stage1Data$type[which(Stage1Data$id %in% Control@id)]="Control";    Stage1Data$type[which(Stage1Data$id %in% Checks@id)]="Checks"; 
    
    Stage1Data$yearOrigin <- year - 4
    Stage1Container[[counter]] <-  Stage1
    
    # Year 3 (F4F5)
    F4F5 = F2F3
    for(u in 1:2){F4F5 <- self(F4F5, nProgeny = 1, parents = NULL, simParam = NULL)} # year 3
    
    # Year 2 (F2F3)
    F2F3 = F1 # year 1
    for(u in 1:2){F2F3 <- self(F2F3, nProgeny = 1, parents = NULL, simParam = NULL)} # year 2
    
    # Year 1 (Crossing)
    
    F1 = randCross(Parents, nCrosses=100, nProgeny=20)
    
    yearData[[counter]] <- rbind(EliteData, Stage3Data, Stage2Data, Stage1Data); counter <- counter+1
  }
  yearData <- do.call(rbind, yearData); yearData$iteration <- REP#paste0("BASE_",REP)
  
  ## Create tibble for final output
  # Mean centered on final burn-in year
  # Variance standardized to percentage of final burn-in year
  # output = tibble(scenario=rep("BASE",40),
  #                 year=-19:20,
  #                 gain=meanStage1-meanStage1[20],
  #                 relVar=varStage1/varStage1[20]*100)
  
  path <- file.path(path0,"breeding-program-assessment/check-strategy/dataChecksRenewed");path
  
  saveRDS(yearData,file=file.path(path,paste0(scenarioName,"_",REP,".rds")))

  save(ParentsContainer, Stage1Container, Stage2Container, Stage3Container, EliteContainer, 
       file=file.path(path,paste0(scenarioName,"_",REP,".RData")))
}






